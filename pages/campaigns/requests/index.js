import React, { Component } from "react";
import { Button, Table } from "semantic-ui-react";
import { Link } from "../../../routes";
import Layout from "../../../components/Layout";
import RequestRow from "../../../components/RequestRow";
import Campaign from "../../../ethereum/campaign";
import web3 from "../../../ethereum/web3";

class RequestIndex extends Component {
    static async getInitialProps(props) {
        const { address } = props.query;
        const campaign = Campaign(address);
        const requestCount = await campaign.methods.getRequestsCount().call();
        const contributers = await campaign.methods.contributers().call();
        const accounts = await web3.eth.getAccounts();
        const requests = await Promise.all(
            Array(parseInt(requestCount))
                .fill()
                .map((element, index) => {
                    return campaign.methods.requests(index).call();
                })
        );

        return {
            address,
            requests,
            requestCount,
            contributers,
            campaign,
            accounts
        };
    }

    onApprove = async id => {
        await this.props.campaign.methods
            .approveRequest(id)
            .send({ from: this.props.accounts[0] });
    };

    onFinalize = async id => {
        console.log(this.props.campaign.methods);
        await this.props.campaign.methods
            .finalizeRequest(id)
            .send({ from: this.props.accounts[0] });
    };

    renderRow() {
        return this.props.requests.map((request, index) => {
            return (
                <RequestRow
                    request={request}
                    key={index}
                    id={index}
                    onApprove={this.onApprove}
                    onFinalize={this.onFinalize}
                    address={this.props.address}
                    contributers={this.props.contributers}
                />
            );
        });
    }

    render() {
        const { Header, Row, HeaderCell, Body } = Table;

        return (
            <Layout>
                <h3>Request List</h3>
                <Link route={`/campaigns/${this.props.address}/requests/new`}>
                    <a>
                        <Button
                            primary
                            floated="right"
                            style={{ marginBottom: 10 }}
                        >
                            Add Request
                        </Button>
                    </a>
                </Link>
                <Table>
                    <Header>
                        <Row>
                            <HeaderCell>ID</HeaderCell>
                            <HeaderCell>Description</HeaderCell>
                            <HeaderCell>Amount</HeaderCell>
                            <HeaderCell>Recepient</HeaderCell>
                            <HeaderCell>Approval Count</HeaderCell>
                            <HeaderCell>Approve</HeaderCell>
                            <HeaderCell>Finalize</HeaderCell>
                        </Row>
                    </Header>
                    <Body>{this.renderRow()}</Body>
                </Table>
                <div>Found {this.props.requestCount} Requests</div>
            </Layout>
        );
    }
}
export default RequestIndex;
