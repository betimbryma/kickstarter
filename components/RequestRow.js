import React, { Component } from "react";
import { Table, Button } from "semantic-ui-react";
import web3 from "../ethereum/web3";
import Campaign from "../ethereum/campaign";

class RequestRow extends Component {
    onApprove = async () => {
        await this.props.onApprove(this.props.id);
    };

    onFinalize = async () => {
        await this.props.onFinalize(this.props.id);
    };

    render() {
        const { id, request, contributers } = this.props;
        const { Row, Cell } = Table;
        const readyToFinalize = request.approvalCount > contributers / 2;
        return (
            <Row
                disabled={request.complete}
                positive={readyToFinalize && !request.complete}
            >
                <Cell>{id}</Cell>
                <Cell>{request.description}</Cell>
                <Cell>{web3.utils.fromWei(request.value, "ether")}</Cell>
                <Cell>{request.recipient}</Cell>
                <Cell>
                    {request.approvalCount}/{contributers}
                </Cell>
                <Cell>
                    <Button
                        disabled={request.complete}
                        color="green"
                        basic
                        onClick={this.onApprove}
                    >
                        Approve
                    </Button>
                </Cell>
                <Cell>
                    <Button
                        disabled={request.complete}
                        color="orange"
                        basic
                        onClick={this.onFinalize}
                    >
                        Finalize
                    </Button>
                </Cell>
            </Row>
        );
    }
}

export default RequestRow;
