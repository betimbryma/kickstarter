const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const compiledFactory = require("./build/CampaignFactory.json");

const provider = new HDWalletProvider(
  "slow ready click bridge eager belt rude apology supply panel weird hold",
  "https://rinkeby.infura.io/v3/68a1b7f17ab34d1a97419212bcdf48b4"
);
const web3 = new Web3(provider);
console.log(web3.version);
console.log(provider.version);
let accounts;
let factory;

const deploy = async () => {
  accounts = await web3.eth.getAccounts();
  factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({ data: "0x" + compiledFactory.bytecode })
    .send({ from: accounts[0], gas: "1000000" });
  console.log("Contract deployed to", factory.options.address);
};

deploy();
